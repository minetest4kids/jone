# jone

stable-branch
Version: 1.0

'jone' ist ein [Hugo](https://gohugo.io/) Theme Fork von [Jane](https://github.com/xianmin/hugo-theme-jane).

## Shortcodes

### Notice
from [https://github.com/martignoni/hugo-notice](https://github.com/martignoni/hugo-notice)
```
{{< notice warning >}}
This is a warning notice. Be warned!
{{< /notice >}}
```

### OpenStreetMap Shortcodes
from https://github.com/hanzei/hugo-component-osm
```
{{< openstreetmap mapName="demo-map_1" >}}
```

### Tabs
from https://hugo-coder.netlify.app/posts/html-and-css-only-tabs/
```
{{< tabgroup >}}
  {{< tab name="Hello" >}}
  Hello World!
  {{< /tab >}}

  {{< tab name="Goodbye" >}}
  Goodbye Everybody!
  {{< /tab >}}
{{< /tabgroup >}}
```

### Gallery
from https://github.com/mfg92/hugo-shortcode-gallery
```
{{< gallery match="images/*" sortOrder="desc" rowHeight="150" margins="5" thumbnailResizeOptions="600x600 q90 Lanczos" showExif=true previewType="blur" embedPreview=true loadJQuery=true >}}
```




[Lizenz](LICENSE)
