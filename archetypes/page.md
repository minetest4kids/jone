+++
title = "{{ replace .Name "-" " " | title }}" 
date = {{ .Date }}
lastmod = {{ .Date }}
draft = true

toc = false
contentCopyright = '<a rel="license noopener" href="https://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank">CC BY-NC-ND 4.0</a>'
+++


